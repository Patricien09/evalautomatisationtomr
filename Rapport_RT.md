---
title: Rapport Automatisation
author: Tom Rouillon
date: 10/01/2024
...

<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<style>
h1 {
    text-align: center;
}
body {
   font-family: "Oxygen", sans-serif;
   font-size: 12pt;
   max-width: 80%;
   margin: auto;
   text-align: justify;
}
nav {
    display: none;
}
</style>
<body>

Lien vers le gitlab : https://gitlab.com/Patricien09/evalautomatisationtomr

# Programme python et PHP

## Python

Il y a 2 fichiers pyhton, un pour définir la fonction de calcul du volume d'une sphère, de sauvegarde des informations et le programme principal qui demande à l'utilisateur de rentrer le rayon de la sphère. L'autre fichier est pour les tests unitaires.

La sauvegarde des informations se fait dans un fichier texte, qui est créé si il n'existe pas. S'il existe, les informations déjà présentes sont écrasées. Ainsi, le programme PHP n'aura qu'à lire le fichier directement.

Il y a 4 tests :

- Rayon positif
- Rayon négatif, où le programme prend la valeur absolue
- Rayon nul, où le programme remplace par 0
- Rayon égal à 0

Pour le lint, j'ai utilisé pylint, qui doit préalablement être installé avec la commande `pip install pylint`. Ensuite, il faut lancer la commande `pylint sphere_vol_cal.py` pour avoir le résultat.

Enfin, pour le codestyle, j'ai utilisé pycodestyle, qui doit préalablement être installé avec la commande `pip install pycodestyle`. Ensuite, il faut lancer la commande `pycodestyle sphere_vol_cal.py` pour avoir le résultat.

## PHP

Pour le PHP, c'est juste une simple page qui lit le fichier `result.txt` et affiche le contenu de celui-ci.

# Gitlab CI/CD

Dans le fichier `.gitlab-ci.yml`, j'ai défini 3 stages :

- build : concerne le build de l'image docker
- test : comprend les tests unitaires et le lint
- deploy : ne fais rien de particulier

Pour les tests et le lint, j'ai utilisé l'image `python:3` et `php`. A partir de là, j'ai pu installer pylint et pycodestyle avec pip, puis lancer les commandes de lint. Pour les tests, il suffit de lancer la commande `python3 test_sphere_vol_cal.py`. Pour le lint PHP, j'ai utilisé la fonction incluse dans PHP, `php -l index.php`.

J'ai autorisé les jobs de lint à échouer, car il n'est pas nécessaire que le programme soit parfaitement codé pour qu'il fonctionne. Cependant, les tests unitaires doivent être réussis pour que le programme fonctionne.

Pour la construction de l'image Docker, j'ai utilisé un job lors de la phase de build à partir de l'image docker officielle. J'ai pu donc utiliser la commande `docker build -t sphere_vol_cal .` pour construire l'image à partir du Dockerfile du projet. Ensuite, Pour pouvoir la télécharger, j'ai utilisé la commande `docker save` pour sauvegarder l'image dans un fichier tar, puis `artifacts` pour pouvoir la télécharger à partir de gitlab.

Dans le Dockerfile, j'utilise l'image php-apache, puis j'installe python3. Je copie les fichiers sources dans le dossier `/var/www/html/` pour pouvoir les utiliser avec apache. J'expose ensuite le port 9090 pour pouvoir accéder au site web. Enfin, je lance apache avec la commande `apache2-foreground`.

Pour la phase de déploiement, j'ai utilisé un job qui utilise l'image docker officielle, puis qui télécharge l'image construite précédemment. Ensuite, il faut lancer la commande `docker load -i sphere_vol_cal.tar` pour charger l'image dans docker. Enfin, il faut lancer la commande `docker run -d -p 9090:80 sphere_vol_cal` pour lancer le conteneur.

Il est également possible de faire ces commandes en téléchargeant l'artifact, puis en extrayant l'image.

On peut ensuite faire un `docker exec -it <container_id> sh` pour accéder au conteneur et lancer le programme python.

Ainsi, quand on va sur `localhost:9090`, on peut voir le résultat du programme python.

</body>