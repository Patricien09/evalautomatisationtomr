<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <title>Résultats</title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="stylesheet" href="">
    </head>
    <body>
        <h2>Résultats</h2>
        <p>
            <?php
                $result_file = fopen("./ressources/result.txt", "r") or die("Impossible d'ouvrir le fichier");
                while (!feof($result_file)) {
                    $line = fgets($result_file);
                    $key = explode(":", $line)[0];
                    $value = explode(":", $line)[1];
                    echo "<p>" . $key . " : " . $value . "</p>";
                }
                fclose($result_file);
            ?>
    </body>
</html>
