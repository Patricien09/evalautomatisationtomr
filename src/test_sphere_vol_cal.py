"""
Tests unitaires pour le calcul du volume d'une sphère
"""

import unittest
import sys
from sphere_vol_cal import calculate_sphere_volume

sys.path.insert(1, '../')


class TestSphereVolCal(unittest.TestCase):
    """
    Classe de tests unitaires pour le calcul du volume d'une sphère.
    """

    def test_positive_radius(self):
        """
        Teste le calcul du volume d'une sphère avec un rayon positif.
        """
        self.assertEqual(calculate_sphere_volume(5), 523.60)

    def test_negative_radius(self):
        """
        Teste le calcul du volume d'une sphère avec un rayon négatif.
        """
        self.assertEqual(calculate_sphere_volume(-5), 523.60)

    def test_zero_radius(self):
        """
        Teste le calcul du volume d'une sphère avec un rayon égal à zéro.
        """
        self.assertEqual(calculate_sphere_volume(0), 0.00)

    def test_null_radius(self):
        """
        Teste le calcul du volume d'une sphère avec un rayon nul.
        """
        self.assertEqual(calculate_sphere_volume(None), 0.00)


if __name__ == '__main__':
    unittest.main()
