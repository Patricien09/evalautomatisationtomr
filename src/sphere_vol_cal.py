"""
Calcule le volume d'une sphère en fonction de son rayon.
Le rayon est demandé à l'utilisateur.
"""

import math
import os


def calculate_sphere_volume(radius):
    """
    Calcule le volume d'une sphère en fonction de son rayon.
    :param radius: le rayon de la sphère
    :return: le volume de la sphère, arrondi à 2 décimales
    """
    if radius is None:
        radius = 0

    if radius < 0:
        radius = abs(radius)

    volume = (4/3) * math.pi * radius ** 3
    return round(volume, 2)


def save_to_file(volume, radius):
    """
    Sauvegarde le volume et le rayon de la sphère dans un fichier texte.
    :param volume: le volume de la sphère
    :param radius: le rayon de la sphère
    """
    script_dir = os.path.dirname(__file__)
    abs_file_path = os.path.join(script_dir, "ressources", "result.txt")
    with open(abs_file_path, "w", encoding='UTF8') as file:
        file.write(f"Rayon:{radius}cm\n")
        file.write(f"Volume:{volume}cm3")


def main():
    """
    Fonction principale du programme.
    Demande le rayon de la sphère à l'utilisateur, calcule son volume
    et l'enregistre dans un fichier texte.
    """
    try:
        radius = float(input("Entrez le rayon de la sphère, en cm : "))
    except ValueError:
        print("Vous devez entrer un nombre !")
        return

    volume = calculate_sphere_volume(radius)
    print(f"Le volume de la sphère est de {volume} cm3")
    save_to_file(volume, radius)


if __name__ == "__main__":
    main()
