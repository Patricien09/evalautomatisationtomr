FROM php:8.1-apache

RUN apt-get update && apt-get install -y python3

COPY "./src" "/var/www/html"

EXPOSE 9090

ENTRYPOINT ["apache2-foreground"]